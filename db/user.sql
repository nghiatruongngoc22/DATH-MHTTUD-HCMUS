CREATE DATABASE `dath` /*!40100 DEFAULT CHARACTER SET utf8 */;

CREATE TABLE `dath`.`user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `passphrase` longtext NOT NULL,
  `address` varchar(500) NOT NULL,
  `dob` date NOT NULL,
  `phone` varchar(20) NOT NULL,
  `private_key` varchar(1000) NOT NULL,
  `public_key` varchar(1000) NOT NULL,
  `salt` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `public_key_UNIQUE` (`public_key`),
  UNIQUE KEY `private_key_UNIQUE` (`private_key`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
