/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.*;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author nghiatn
 */
public class AESHandler extends AbstractCipherHandler {

    public AESHandler(byte[] _key) {
        key = _key;
    }

    @Override
    public boolean execute(InputStream is, OutputStream os, int mode) {

        try {
            byte[] decodedKey = key;
            SecretKey desKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv = new IvParameterSpec("0102030405060708".getBytes());

            switch (mode) {
                case ENCRYPT_MODE:
                    cipher.init(Cipher.ENCRYPT_MODE, desKey, iv);
                    CipherInputStream cis = new CipherInputStream(is, cipher);
                    handleIO(cis, os);
                    break;
                default:
                    cipher.init(Cipher.DECRYPT_MODE, desKey, iv);
                    CipherOutputStream cos = new CipherOutputStream(os, cipher);
                    handleIO(is, cos);
                    break;
            }

            return true;
        } catch (Exception e) {
            System.err.println("DES_Encrypter-execute-ERROR: " + e.getMessage());

            return false;
        }
    }

}
