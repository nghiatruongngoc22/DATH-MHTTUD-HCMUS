/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPublicKey;

/**
 *
 * @author User
 */
public class SignatureHelper {
    public static boolean signAFile(PrivateKey key, FileInputStream in, String sigfile) throws NoSuchAlgorithmException, InvalidKeyException, IOException, SignatureException {
        Signature sign = Signature.getInstance("SHA256withRSA");
        sign.initSign(key);
        
            byte[] hashvalue = HashUtlils.hashFile(in);
            if (hashvalue != null)
            {
                sign.initSign(key);
                sign.update(hashvalue);
                byte[] s = sign.sign();
                try (FileOutputStream out = new FileOutputStream(sigfile)){
                    out.write(s);
                }
            }
            else return false;
        return true;
        
    }
    
    public static boolean verifyAFile(PublicKey key, FileInputStream in, String sigfile) throws NoSuchAlgorithmException, InvalidKeyException, IOException, SignatureException {
        Signature sign = Signature.getInstance("SHA256withRSA");
        byte[] hashvalue = HashUtlils.hashFile(in);
        if (hashvalue != null)
        {
            sign.initVerify(key);
            sign.update(hashvalue);
                
            try (FileInputStream sig = new FileInputStream(sigfile)){
                int size;
                size = ((RSAPublicKey)key).getModulus().bitLength() / 8;
                byte[] s = new byte[size];
                sig.read(s);
                return sign.verify(s);
            }
        } else return false;   
    }
}
