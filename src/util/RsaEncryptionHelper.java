/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author User
 */
public class RsaEncryptionHelper {
    
    public static PublicKey getPublicKey(KeyPair key) {
        return key.getPublic();
    }
    
     public static PrivateKey getPrivateKey(KeyPair key) {
        return key.getPrivate();
    }
     
    public static KeyPair buildKeyPair(int keySize) throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(keySize);      
        return keyPairGenerator.genKeyPair();
    }

    public static byte[] encrypt(PublicKey publicKey, SecretKey ks) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");  
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);  

        
        byte[] a =  cipher.doFinal(ks.getEncoded());  
        
        return a;
    }
    
    public static byte[] decrypt(PrivateKey privateKey, byte [] encrypted) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");  
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        
        return cipher.doFinal(encrypted);
    }
    
    public static PrivateKey getPrivateKey(String prv, String pass) throws NoSuchAlgorithmException, InvalidKeySpecException, Exception {

        byte[] data = AESHelper.decrypt(prv, pass);
        PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(data);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PrivateKey pvt = kf.generatePrivate(ks);
        return pvt;
    }
    public static PublicKey getPublicKey(String pub) throws NoSuchAlgorithmException, InvalidKeySpecException, Exception {

        BASE64Decoder decoder = new BASE64Decoder();

        byte[] data = decoder.decodeBuffer(pub);
        X509EncodedKeySpec ks = new X509EncodedKeySpec(data);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PublicKey p = kf.generatePublic(ks);
        return p;
    }
    
    public static String convertPublicKey(PublicKey p) {
        byte[] publicKeyBytes = p.getEncoded();
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(publicKeyBytes);
    }
}
