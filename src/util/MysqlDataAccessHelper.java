/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.mysql.jdbc.Driver;
import com.mysql.jdbc.Statement;
import dto.UserDTO;
import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author cdphuc
 */
public class MysqlDataAccessHelper {

    public Connection conn = null;
    // xu ly ngoai le khi tuong tac voi csdl

    public void displayError(SQLException ex) {
        System.out.println(" Error Message:" + ex.getMessage());
        System.out.println(" SQL State:" + ex.getSQLState());
        System.out.println(" Error Code:" + ex.getErrorCode());
    }

    public void open() {// mo ket noi den csdl
        try {
            Driver driver = new org.gjt.mm.mysql.Driver();// nap driver
            DriverManager.registerDriver(driver);// dang ky driver

            String url = "jdbc:mysql://localhost:3306/DATH";
            conn = DriverManager.getConnection(url, "root", "ngoc1207");//tao ket noi den co so du lieu

        } catch (SQLException ex) {// xu ly ngoai le
            displayError(ex);
        }
    }

    public void close() {// dong ket noi co so du lieu
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            displayError(ex);
        }
    }
    //tao va thuc thi cac cau lenh sql
    // cung cap thong tin trich rut tu csdl va cho phep truy xuat tung dong du lieu

    public HashMap<String, String> excuteSelectQuery() {// danh cho cau lenh secect
        HashMap<String, String> list = new HashMap<String, String>();
        ResultSet rs = null;
        String sql = "select email, public_key from user";
        try {
            Statement stm = (Statement) conn.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                list.put(rs.getString("public_key"), rs.getString("email"));
            }
        } catch (SQLException ex) {
            displayError(ex);
        }
        return list;
    }

    public UserDTO excuteSelectAUser(String email) throws SQLException {// danh cho cau lenh secect
        ResultSet rs = null;
        String sql = "select * from user where email = ?";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, email);
        try {
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                UserDTO user = new UserDTO();
                user.setEmail(rs.getString("email"));
                user.setName(rs.getString("name"));
                user.setAddress(rs.getString("address"));
                user.setPhone(rs.getString("phone"));
                user.setPrivateKey(rs.getString("private_key"));
                user.setPublicKey(rs.getString("public_key"));
                user.setSalt(rs.getString("salt"));
                user.setPassphrase(rs.getString("passphrase"));
                user.setDob(rs.getDate("dob"));
                return user;
            }
        } catch (SQLException ex) {
            displayError(ex);
        }
        return null;
    }

    public Map<String, UserDTO> excuteSelectUsersNotHaveEmail(String email) throws SQLException {// danh cho cau lenh secect
        ResultSet rs = null;
        String sql = "select email, name, public_key from user where email != ?";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, email);

        Map<String, UserDTO> result = new HashMap<>();
        try {
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                UserDTO user = new UserDTO();
                user.setEmail(rs.getString("email"));
                user.setName(rs.getString("name"));
                user.setPublicKey(rs.getString("public_key"));
                result.put(user.getEmail(), user);
            }
        } catch (SQLException ex) {
            displayError(ex);

            //return null;
        }
        return result;
    }

    public int excuteUpdateKeyPair(String email, String publicKey, String privateKey) throws SQLException {
        String sql = "update user set private_key = ?, public_key = ? where email = ?";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);

        preparedStatement.setString(1, privateKey);
        preparedStatement.setString(2, publicKey);
        preparedStatement.setString(3, email);
        return preparedStatement.executeUpdate();
    }

    public int excuteUpdateAUser(String name, String address, String phone, Date dob, String passphrase, String email, String privateKey, String salt) throws SQLException {// danh cho cau lenh secect
        String sql = "update user set name = ?, address = ?, phone = ?, dob = ?, passphrase = ?, salt = ?, private_key = ? where email = ?";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
        String s = timeFormat.format(dob);
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, address);
        preparedStatement.setString(3, phone);
        preparedStatement.setString(4, s);
        preparedStatement.setString(5, passphrase);
        preparedStatement.setString(6, salt);
        preparedStatement.setString(7, privateKey);
        preparedStatement.setString(8, email);

        return preparedStatement.executeUpdate();
    }

    public int excuteInsertQuery(UserDTO user) {// danh cho cau lenh secect
        String query = " insert into user (email,passphrase,name,address,dob,phone,public_key,private_key, salt)"
                + " values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
        String s = timeFormat.format(user.getDob());
        try {
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, user.getEmail());
            preparedStmt.setString(2, user.getPassphrase());
            preparedStmt.setString(3, user.getName());
            preparedStmt.setString(4, user.getAddress());
            preparedStmt.setString(5, s);
            preparedStmt.setString(6, user.getPhone());
            preparedStmt.setString(7, user.getPublicKey());
            preparedStmt.setString(8, user.getPrivateKey());
            preparedStmt.setString(9, user.getSalt());
            preparedStmt.execute();
        } catch (SQLException ex) {
            System.out.print(ex.getMessage());
            if (ex.getErrorCode() == 1062) {
                if (ex.getMessage().endsWith("'email_UNIQUE'")) {
                    return 2;
                } else if (ex.getMessage().endsWith("'private_key_UNIQUE'")) {
                    return 3;
                } else if (ex.getMessage().endsWith("'public_key_UNIQUE'")) {
                    return 4;
                }
            }
            return 5;
        }
        return 1;
    }
}
