/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author lap10509
 */
public abstract class AbstractCipherHandler {
    public static final int ENCRYPT_MODE = 1;
    public static final int DECRYPT_MODE = 2;

    protected byte[] key;
    
    public abstract boolean execute(InputStream is, OutputStream os, int mode);
    
    public void handleIO(InputStream is, OutputStream os) throws IOException {
        byte[] bytes = new byte[64];
        int numBytes;
        while ((numBytes = is.read(bytes)) != -1) {
            os.write(bytes, 0, numBytes);
        }
        os.flush();
        os.close();
        is.close();
    }
}
