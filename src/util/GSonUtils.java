/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author nghiatn
 */
public class GSonUtils<T> {

    private static Gson gson;

    public static String object2JsonStr(Object t) {
        gson = new Gson();
        return gson.toJson(t);
    }

    public static <T> T fromJsonString(String json, Class<T> classOfT) {
        gson = new Gson();
        return gson.fromJson(json, classOfT);
    }

    public static boolean exportObjectToFile(Object obj, String filePath) {

        //Usage
        // boolean a = GSonUtils.exportObjectToFile(user, "./aa.json");
        gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssz")
                .create();
        try {
            String json = gson.toJson(obj);
            FileWriter writer = new FileWriter(filePath);
            writer.write(json);
            writer.close();
            return true;
        } catch (IOException ex) {
            System.err.println("GSonUtils-exportObjectToFile-ERROR: " + ex.getMessage());
            return false;
        }
    }

    public static <T> T importFromFile(String filePath, Class<T> classOfT) {
        // Usage:
        //user = GSonUtils.importFromFile("./aa.json", UserDTO.class);

        gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssz")
                .create();
        try {
            return gson.fromJson(new FileReader(filePath), classOfT);
        } catch (Exception ex) {
            System.err.println("GSonUtils-importFromFile-ERROR: " + ex.getMessage());
            return null;
        }
    }
}
