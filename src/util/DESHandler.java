/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.*;
import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;

/**
 *
 * @author lap10509
 */
public class DESHandler extends AbstractCipherHandler {

    public DESHandler(byte[] _key) {
        key = _key;
    }

    @Override
    public boolean execute(InputStream is, OutputStream os, int mode) {

        try {
            DESKeySpec dks = new DESKeySpec(key);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
            SecretKey desKey = skf.generateSecret(dks);
            Cipher cipher = Cipher.getInstance("DES");

            switch (mode) {
                case ENCRYPT_MODE:
                    cipher.init(Cipher.ENCRYPT_MODE, desKey);
                    CipherInputStream cis = new CipherInputStream(is, cipher);
                    handleIO(cis, os);
                    break;
                default:
                    cipher.init(Cipher.DECRYPT_MODE, desKey);
                    CipherOutputStream cos = new CipherOutputStream(os, cipher);
                    handleIO(is, cos);
                    break;
            }

            return true;
        } catch (Exception e) {
            System.err.println("DES_Encrypter-execute-ERROR: " + e.getMessage());

            return false;
        }
    }

}
