/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.security.Key;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author User
 */
public class AESHelper {
    public static SecretKeySpec convertToSecretKey(String passphrase)throws Exception {
        byte[] key = (passphrase).getBytes("UTF-8");
        key = Arrays.copyOf(key, 16);
        System.out.print(key.length);
        SecretKeySpec aesKey = new SecretKeySpec(key, "AES");
        return aesKey;
    }
    public static String encrypt(byte[] message, String passphrase) throws Exception {
        SecretKeySpec aesKey = convertToSecretKey(passphrase);
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, aesKey);  
        byte[] encrypted=cipher.doFinal(message);
        String strData=new BASE64Encoder().encode(encrypted);
        return strData;  
    }
    
    public static byte[] decrypt(String encrypted, String passphrase) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        Key aesKey = convertToSecretKey(passphrase);
        cipher.init(Cipher.DECRYPT_MODE, aesKey);
        byte[] decordedValue = new BASE64Decoder().decodeBuffer(encrypted);
        byte[] decValue = cipher.doFinal(decordedValue);
        return decValue;
    }
}
