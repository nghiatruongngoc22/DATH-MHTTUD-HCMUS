/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import dto.UserDTO;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author nghiatn
 */
public class HashUtlils {

    public static String hash(String plainText, String salt) {
        return BCrypt.hashpw(plainText, salt);
    }

    public static boolean verifyHash(String plainText, String hash) {
        return BCrypt.checkpw(plainText, hash);
    }

    public static String getNextSalt() {
        return BCrypt.gensalt();
    }

    public static byte[] hashFile(FileInputStream file) throws NoSuchAlgorithmException {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] dataBytes = new byte[1024];
            int bytesFromFile;
            while ( (bytesFromFile=file.read(dataBytes)) != -1 ) {
                md.update(dataBytes,0,bytesFromFile);
            };
            byte byteData[] = md.digest();

            return byteData;
        } catch (IOException ex) {
            Logger.getLogger(HashUtlils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
//    public static void main(String argsp[]) {
//        UserDTO user= new UserDTO();
////        
////        user.setAddress("address");
////        user.setPassphrase("aaa");
//         user = GSonUtils.importFromFile("./aa.json", UserDTO.class);
//        
//        System.err.println(user);
//    }
    public static void main(String argsp[]) {
        UserDTO user= new UserDTO();
        
        user.setAddress("address");
        user.setPassphrase("aaa");
        boolean a = GSonUtils.exportObjectToFile(user, "./aa.json");
        
        System.err.println(a);
    }

}
