/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dto.UserDTO;
import java.util.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import util.MysqlDataAccessHelper;

/**
 *
 * @author User
 */
public class UserDao {

    static MysqlDataAccessHelper my = new MysqlDataAccessHelper();

    public static int CreateUser(UserDTO user) {
        my.open();
        int result = my.excuteInsertQuery(user);
        my.close();
        return result;
    }

    public static int UpdateUser(String name,
            String address, String phone, Date dob,
            String passphrase, String email,
            String salt, String privateKey) throws SQLException {
        my.open();
        int result = my.excuteUpdateAUser(name, address, phone, dob, passphrase, email, salt, privateKey);
        my.close();
        return result;
    }

    public static int updateKeyPair(String email, String publicKey, String privateKey) throws SQLException {
        my.open();
        int result = my.excuteUpdateKeyPair(email, publicKey, privateKey);
        my.close();
        return result;
    }

    public static UserDTO GetUser(String email) throws SQLException {
        my.open();
        UserDTO user = my.excuteSelectAUser(email);
        my.close();
        return user;
    }

    public static HashMap<String, String> GetListUser() throws SQLException {
        my.open();
        HashMap<String, String> list = my.excuteSelectQuery();
        my.close();
        return list;
    }

    public static Map<String, UserDTO> getUsersExceptEmail(String email) throws SQLException {
        my.open();
        Map<String, UserDTO> user = my.excuteSelectUsersNotHaveEmail(email);
        my.close();
        return user;
    }
}
