/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bus;

import dao.UserDao;
import dto.UserDTO;
import java.util.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class UserBus {
    public static int CreateUser(UserDTO user){
        return UserDao.CreateUser(user);
    }
    
    public static UserDTO GetUser(String email) throws SQLException{
        return UserDao.GetUser(email);
    }
    
    public static int UpdateUser(String name, String address, String phone, Date dob, String passphrase, String email, String salt, String privateKey) throws SQLException{
        return UserDao.UpdateUser(name, address, phone, dob, passphrase, email, salt, privateKey);
    }
    
    public static int updateKeyPair(String email, String publicKey, String privateKey) throws SQLException{
        return UserDao.updateKeyPair(email, publicKey, privateKey);
    }
    public static HashMap<String, String> GetListUser() throws SQLException{
        return UserDao.GetListUser();
    }
    
    
    public static Map<String, UserDTO> GetUserExceptEmail(String email) {
        try {
            return UserDao.getUsersExceptEmail(email);
        } catch (SQLException ex) {
            Logger.getLogger(UserBus.class.getName()).log(Level.SEVERE, null, ex);
            return new HashMap<>();
        }
    }
}
