/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bus;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import org.apache.commons.lang.ArrayUtils;
import util.AESHandler;
import util.AbstractCipherHandler;
import util.DESHandler;
import util.RsaEncryptionHelper;

/**
 *
 * @author nghiatn
 */
public class FileHandleBus {

    private static AbstractCipherHandler cipherHandle = null;

    public static final ArrayList<String> SUPPORT_EA = new ArrayList<String>() {
        {
            add("AES");
            add("DES");
        }
    };

    public static final String AES = "AES";
    public static final String DES = "DES";

    public static final int MARK_EA_SIZE = 1;
    public static final int MARK_KEY_LENGTH_SIZE = 1;

    public static final String ENCRYPT_EXTENSION = "_encrypted.22";

    public static final int MAX_FILE_SIZE = 2000000;

    public static boolean checkFileSize(String filePath) {
        File file = new File(filePath);
        if (file.length() > MAX_FILE_SIZE) {
            return false;
        }

        return true;
    }

    public static boolean encryptFile(String filePath, String receiverPublicKey, String encryptAlgorithm) {

        try {

            KeyGenerator keyGen = KeyGenerator.getInstance(AES);
            SecretKey secretKey = keyGen.generateKey();

            byte[] encryptedKey = RsaEncryptionHelper
                    .encrypt(RsaEncryptionHelper
                            .getPublicKey(receiverPublicKey), secretKey);

            System.out.println("Executing...............");

            storeEncryptedKeyS(filePath, encryptedKey, encryptAlgorithm);

            FileInputStream fis = new FileInputStream(filePath);
            FileOutputStream fos = new FileOutputStream(filePath + ENCRYPT_EXTENSION, true);

            switch (encryptAlgorithm) {
                case DES:
                    cipherHandle = new DESHandler(secretKey.getEncoded());
                    cipherHandle.execute(fis, fos, AbstractCipherHandler.ENCRYPT_MODE);
                    break;
                case AES:
                    cipherHandle = new AESHandler(secretKey.getEncoded());
                    cipherHandle.execute(fis, fos, AbstractCipherHandler.ENCRYPT_MODE);
                    //
                    break;
            }
            System.out.println("Done...............");
        } catch (Exception e) {
            System.err.println("DES_AES_Encryption-something's wrong: " + e.getMessage());
            return false;
        }

        return true;
    }

    private static void storeEncryptedKeyS(String filePath, byte[] encrypted, String encryptAlgorithm) throws Exception {
        File yourFile = new File(filePath + ENCRYPT_EXTENSION);
        yourFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(yourFile);

        fos.write(encrypted.length == 128 ? 0 : encrypted.length);
        fos.write(encrypted);

        switch (encryptAlgorithm) {
            case DES:
                fos.write(1);
                break;
            case AES:
                fos.write(2);
                break;
        }

        fos.close();
    }

    private static byte[] getStoredKey(String filePath) throws Exception {
        byte[] buffer = new byte[MARK_KEY_LENGTH_SIZE];
        InputStream is = new FileInputStream(filePath);
        if (is.read(buffer) != buffer.length) {
            is.close();
            return null;
        }

        Byte[] byteObject = ArrayUtils.toObject(buffer);

        int encryptedKeyLength = byteObject[0].intValue();
        
        if (encryptedKeyLength == 0) {
            encryptedKeyLength = 128;
        }

        buffer = new byte[encryptedKeyLength];

        if (is.read(buffer) != buffer.length) {
            is.close();
            return null;
        }

        is.close();

        return buffer;
    }

    private static byte[] getEncryptedData(int keySize, String filePath) throws Exception {
        InputStream is = new FileInputStream(filePath);

        // skip header which contains encryptedKey and 2 bytes of markers.
        is.skip(keySize + MARK_EA_SIZE + MARK_KEY_LENGTH_SIZE);

        File yourFile = new File(filePath);

        int dataSize = (int) yourFile.length() - keySize - MARK_EA_SIZE - MARK_KEY_LENGTH_SIZE;

        byte[] buffer = new byte[dataSize];

        if (is.read(buffer) != buffer.length) {
            is.close();
            return null;
        }
        is.close();

        return buffer;
    }

    private static byte[] getAEMark(int keySize, String filePath) throws Exception {
        InputStream is = new FileInputStream(filePath);
        is.skip(keySize + MARK_EA_SIZE);

        byte[] buffer = new byte[MARK_EA_SIZE];

        if (is.read(buffer) != buffer.length) {
            is.close();
            return null;
        }
        is.close();

        return buffer;
    }

    public static void main(String args[]) throws Exception {
        getStoredKey("/Volumes/Data/DATH-MHTTUD-HCMUS/myprofilea.json_encrypted.22");
    }

    public static boolean decryptFile(String filePath, String privateKey, String passphrase) {

        try {
            byte[] storedEncryptedKey = getStoredKey(filePath);

            byte[] keyS = RsaEncryptionHelper.decrypt(
                    RsaEncryptionHelper.getPrivateKey(privateKey, passphrase),
                    storedEncryptedKey);

            byte[] aeMark = getAEMark(storedEncryptedKey.length, filePath);
            byte[] encryptedData = getEncryptedData(storedEncryptedKey.length, filePath);

            Byte[] aeMarkObj = ArrayUtils.toObject(aeMark);

            File newFile = new File(filePath.replace(ENCRYPT_EXTENSION, ""));
            newFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(filePath.replace(ENCRYPT_EXTENSION, ""), false);

            String encryptAlgorithm;
            if (aeMarkObj[0].intValue() == 1) {
                encryptAlgorithm = DES;
            } else {
                encryptAlgorithm = AES;
            }

            switch (encryptAlgorithm) {
                case DES:
                    cipherHandle = new DESHandler(keyS);
                    cipherHandle.execute(new ByteArrayInputStream(encryptedData),
                            fos, AbstractCipherHandler.DECRYPT_MODE);
                    break;
                case AES:
                    cipherHandle = new AESHandler(keyS);
                    cipherHandle.execute(new ByteArrayInputStream(encryptedData),
                            fos, AbstractCipherHandler.DECRYPT_MODE);
                    break;
            }
            System.out.println("Done...............");

        } catch (Exception e) {
            System.err.println("DES_AES_Encryption-something's wrong: " + e.getMessage());
            return false;
        }

        return true;
    }

    public static boolean checkFileExist(String filePath) {
        File f = new File(filePath);
        if (f.exists() && !f.isDirectory()) {
            return true;
        }
        return false;
    }
}
